using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraSwitcher : MonoBehaviour
{
    public Camera mainCamera;
    public Camera camera2;
    public Camera camera3;

 // Pour  trace  la caméra active
    private int currentCameraIndex = 0;
    private Camera[] cameras;


  
    void Start()
    {
        // Stocker toutes les caméras
        cameras = new Camera[] { mainCamera, camera2, camera3 };

        // the main camera is active from the start 
        SwitchCamera(currentCameraIndex);
        
    }

   
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.RightCommand))
        {
            // Move to the next camera
            currentCameraIndex++;
            
            // puts all the cameras in the loop
            if (currentCameraIndex >= cameras.Length)
            {
                currentCameraIndex = 0;
            }

            // Switch to the new camera
            SwitchCamera(currentCameraIndex);
        }
    }

    // Fonction permettant d'activer la caméra sélectionnée
    private void SwitchCamera(int cameraIndex)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].enabled = (i == cameraIndex);
        }
    }
}
