using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompteurCube : MonoBehaviour
{
    // Counter for cubes
    private int cubeCompteur = 0;

    // Called when another collider enters the trigger
    private void OnTriggerEnter(Collider other)
    {
        // Check if the object has the "Cube" tag
        if (other.CompareTag("Cube"))
        {
            cubeCompteur++;
            // Display the result in the console
            //Debug.Log("Cubes in zone: " + cubeCompteur);
        }            
        Debug.Log("Cubes in zone: " + cubeCompteur);
    }

    // Called when another collider exits the trigger
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Cube"))
        {
            cubeCompteur--;
            Debug.Log("Cubes in zone: " + cubeCompteur);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
