using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
        Destroy(this.gameObject.GetComponent<FixedJoint>());
        }
    } 
    
    //Celle ci détecte quand un objet entre en contact avec un autre pourvu que les deux objets contiennent un composant “collider”
    void OnCollisionEnter(Collision  Collision)
    {
    if (Collision.gameObject.GetComponent<ArticulationBody>() != null) {
    FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
    joint.connectedArticulationBody = Collision.articulationBody; }
    }

     //Cela permet à la grue de “lâcher” l’objet en détruisant le joint qui relie l’objet à la grue
        // if (Input.GetKey(KeyCode.Space)){
        //     Destroy(this.gameObject.GetComponent<FixedJoint>());
        // }
    
        
   
}
